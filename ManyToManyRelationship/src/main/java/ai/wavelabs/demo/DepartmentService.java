package ai.wavelabs.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {
    
	
	@Autowired
	public DepartmentRepository departmentrepository;
	
	public  Department saveDepartment(Department Dept) {
		return departmentrepository.save(Dept);
	}

	public List<Department> getAllDept() {
		List<Department> li = departmentrepository.findAll();
		return li;
	}	
	
	
	
	
}
