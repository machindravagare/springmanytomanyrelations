package ai.wavelabs.demo;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column
	private String bloodgrp;
	@Column
	private String Address;

	public Employee(int id, String bloodgrp, String address) {
		super();
		this.id = id;
		this.bloodgrp = bloodgrp;
		Address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBloodgrp() {
		return bloodgrp;
	}

	public void setBloodgrp(String bloodgrp) {
		this.bloodgrp = bloodgrp;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}
	@ManyToMany(cascade = CascadeType.ALL)
	private Set<Department> departmenttt;

	public Set<Department> getDepartmenttt() {
		return departmenttt;
	}

	public void setDepartmenttt(Set<Department> departmenttt) {
		this.departmenttt = departmenttt;
	}

	

}
